from app.http.cliente_http import ConfiguracionDispositivosIOTClienteHttp
from app.settings import CONFIGURACION_HTTP

cliente_http_vehiculo = ConfiguracionDispositivosIOTClienteHttp(
    host=CONFIGURACION_HTTP['dominio'],
    puerto=CONFIGURACION_HTTP['puerto'],
    segmento='vehiculo',
)

cliente_http_configuracion = ConfiguracionDispositivosIOTClienteHttp(
    host=CONFIGURACION_HTTP['dominio'],
    puerto=CONFIGURACION_HTTP['puerto'],
    segmento='configuracion-dispositivos-iot',
)