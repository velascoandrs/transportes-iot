from abc import ABC
import http.client
import json

# Clase abstracta cliente http
import requests

from app.utils import imprimir_mensaje


class ClienteHTTP(ABC):

    def __init__(self, host, puerto, segmento):
        self.__segmento = segmento
        self.__uri = f'{host}:{puerto}'
        imprimir_mensaje('Se ha configurado el cliente http para: ', self.__segmento, 'info')

    @staticmethod
    def prepare_response(response):
        return response.json()

    def find_one(self, idRegistro, cabeceras=None):
        if cabeceras is None:
            cabeceras = {}
        request_uri = f'{self.__uri}/{self.__segmento}/{idRegistro}'
        imprimir_mensaje(
            f'Realizando peticion: ',
            f'GET {request_uri}',
            'info',
        )
        response = requests.get(f'{request_uri}', headers=cabeceras)
        return self.prepare_response(response)

    def find_all(self, criterios: dict = None, cabeceras: dict = None):
        criterioBusqueda = ''
        if cabeceras is None:
            cabeceras = {}
        if criterios is not None:
            try:
                criterioBusqueda = f'?criterioBusqueda={json.dumps(criterios)}'
            except TypeError as error:
                imprimir_mensaje(
                    'Los parametros de busqueda son invalidos',
                    str(error),
                    tipo='error',
                )
                criterioBusqueda = ''

        request_uri = f'{self.__uri}/{self.__segmento}{criterioBusqueda}'
        imprimir_mensaje(
            f'Realizando peticion: ',
            f'GET {request_uri}',
            'info',
        )
        response = requests.get(
            f'{request_uri}',
            headers=cabeceras
        )
        return self.prepare_response(response)

    def update_one(self, idRegistro, registro, cabeceras=None):
        if cabeceras is None:
            cabeceras = {}
        request_uri = f'{self.__uri}/{self.__segmento}/{idRegistro}'
        imprimir_mensaje(
            f'Realizando peticion: ',
            f'PUT {request_uri} \n BODY {registro}',
            'info',
        )
        response = requests.put(f'{request_uri}', json=registro, headers=cabeceras)
        return self.prepare_response(response)

    def create_one(self, registro: dict, cabeceras=None):
        if cabeceras is None:
            cabeceras = {}
        request_uri = f'{self.__uri}/{self.__segmento}'
        imprimir_mensaje(
            f'Realizando peticion: ',
            f'POST {request_uri} \n   BODY {registro}',
            'info',
        )
        response = requests.post(f'{request_uri}', json=registro, headers=cabeceras)
        return self.prepare_response(response)


class ConfiguracionDispositivosIOTClienteHttp(ClienteHTTP):
    def __init__(self, host, puerto, segmento):
        super(ConfiguracionDispositivosIOTClienteHttp, self) \
            .__init__(host, puerto, segmento)


class VehiculoClienteHttp(ClienteHTTP):
    def __init__(self, host, puerto, segmento):
        super(VehiculoClienteHttp, self) \
            .__init__(host, puerto, segmento)
