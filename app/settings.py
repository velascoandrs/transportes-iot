
CONFIGURACION_WS = {
    'dominio': 'http://localhost',
    'puerto': 3001,
}

CONFIGURACION_MQTT = {
    'dominio': 'localhost',
    'puerto': 32769
}

CONFIGURACION_HTTP = {
    'dominio': 'http://localhost',
    'puerto': 8080
}

TIEMPO_ESPERA_DEFECTO = 60

TOTAL_CAMARAS = 1
