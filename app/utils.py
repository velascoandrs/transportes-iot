import base64
import re
import cv2

from app.constantes.colores import *


def imprimir_implicito(color, mensaje: str, parametros: str = '', tipo: str = 'info') -> None:
    """
    :param parametros:
    :param tipo:
    :param color: codigo del color
    :param mensaje: mensje a mostrar
    :return: None
    """
    switch = {
        'info': '[INFO]',
        'exito': '[SUCCESS]',
        'error': '[ERROR]',
        'notice': '[NOTICE]',
        'aviso': '[NOTICE]'
    }
    tipo_cabecera = switch.get(tipo)
    parametros_imprimir = ''
    if parametros:
        parametros_imprimir = f' {AMARILLO}{parametros}{FIN}' if tipo_cabecera else f'{ROJO}{parametros}{FIN}'
    cabecera = f'{color}{switch.get(tipo)}{FIN} '
    contenido = f'{AZUL}{mensaje}{FIN}' if tipo != 'error' else f'{ROJO}{mensaje}{FIN}'
    contenido = cabecera + contenido + parametros_imprimir
    print(contenido)


# Imprimir en consola con colores
def imprimir_mensaje(mensaje: str, parametros: str = '', tipo: str = 'info') -> None:
    """
    :param parametros:
    :param mensaje: texto a mostrar en consola
    :param tipo: tipo del mensaje: error, info, exito
    :return: None
    """
    switch = {
        'info': CELESTE,
        'exito': VERDE,
        'error': ROJO,
        'aviso': AMARILLO,
        'notice': AMARILLO,
    }
    color = switch.get(tipo, 'info')
    imprimir_implicito(color, mensaje, parametros, tipo)


def tomar_fotografia_camara(indice):
    capturador = cv2.VideoCapture(indice)
    if not capturador.isOpened():
        raise Exception("Could not open video device")
    ret, imagen = capturador.read()
    imagen = cv2.resize(imagen, (128, 128))
    cv2.imwrite('im.jpg', imagen)
    retval, buffer = cv2.imencode('.jpg', imagen)
    jpg_as_text = base64.b64encode(buffer)
    capturador.release()
    imprimir_mensaje('Se tomo una fotografia con la camara: ', str(indice), tipo='info')
    return jpg_as_text


def tomar_fotografia_segura(indice) -> str or False:
    try:
        return tomar_fotografia_camara(indice)
    except:
        imprimir_mensaje('No hay camara detectada en: ', indice, tipo='error')
    return False


def validar_indices_camaras(indices: str):
    return re.match(r"^\d+(,\d+)*$", indices)


def validar_argumentos(placa, indices_camara, totalCamaras):
    if not placa:
        raise Exception(f'{ROJO}[ERROR] La placa del vehiculo es obligatoria {FIN}')
    if len(indices_camara) != int(totalCamaras):
        raise Exception(f'{ROJO}[ERROR] Deben haber solamente {totalCamaras} indice/s de camara{FIN}')
    else:
        indices_validos = validar_indices_camaras(','.join(indices_camara))
        if not indices_validos:
            raise Exception(f'{ROJO}[ERROR] Indices no validos {FIN}')
    return True
