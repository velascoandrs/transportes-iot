import json

import socketio

from app.microservicios import cliente_mqtt_vehiculo
from app.utils import imprimir_mensaje


class VehiculoClienteWS(socketio.ClientNamespace):
    __cliente_mqtt_vehiculo = cliente_mqtt_vehiculo

    def __init__(self, namespace, identificador):
        super().__init__(namespace)
        self._identificador = identificador

    def on_connect(self):
        imprimir_mensaje(f'Se ha conectado: {self.namespace}', tipo='exito')
        self.unirse_sala(nombre_sala=self._identificador)

    def on_disconnect(self):
        imprimir_mensaje(f"Se ha desconectado de:  {self.namespace}", tipo='error')

    def unirse_sala(self, nombre_sala: str):
        self.emit('sala-cabecera', nombre_sala)
        imprimir_mensaje("Emiti mensaje")

    def on_escuchar_cabecera(self, data):
        # Aqui va logica
        try:
            idCabeceraRecorrido = int(data.get('idCRT'))
            imprimir_mensaje(f'Se ha recibido la cabecera:', f'{idCabeceraRecorrido}', tipo='info')
            if idCabeceraRecorrido > 0:
                self.__cliente_mqtt_vehiculo.definir_cabecera_trabajo_recorrido(idCabeceraRecorrido)
                # Logica de asignacion de cabecera recorrido trabajo
                # Se debe setear este valor del idCabeceraRecorrido en el microservicio MQTT
            else:
                if idCabeceraRecorrido == -1:
                    self.disconnect()
        except:
            imprimir_mensaje(f'Datos incorrectos: ', f'{self.namespace}', tipo='error')

    # def trigger_event(self, event, *args):
    #     imprimir_mensaje(event)
    #     super().trigger_event(event, *args)
    #     if event == self._identificador:
    #         self.on_escuchar(*args)
