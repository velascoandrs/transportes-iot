from time import sleep
import socketio
from socketio import exceptions
from app.settings import CONFIGURACION_WS
from app.utils import imprimir_mensaje
from app.websocket import cliente_websocket


def inicializar_websockets(namespaces: type(list)) -> None:
    dominio = CONFIGURACION_WS['dominio']
    puerto = CONFIGURACION_WS['puerto']
    imprimir_mensaje(f"Conectando al websocket: ", f"{dominio}:{puerto}", tipo='info')
    # cliente_websocket.connect(f"{dominio}:{puerto}")
    for namespace in namespaces:
        try:
            cliente_websocket.register_namespace(namespace)
            imprimir_mensaje(f"Se ha registrado el namespace: ", f"{namespace.namespace}", tipo='exito')
        except ValueError as error:
            imprimir_mensaje(f"Error al registrar el ws: \n{error}", tipo='error')
    while True:
        try:
            cliente_websocket.connect(
                f"{dominio}:{puerto}",
                namespaces=
                list(
                    map(
                        lambda nce: nce.namespace,
                        namespaces,
                    ),
                ),
            )
            break
        except socketio.exceptions.ConnectionError:
            imprimir_mensaje(f"No se pudo conectar al servidor: ", f"{dominio}:{puerto}", tipo='error')
            imprimir_mensaje(f"Reitentando conectar al websocket: ", f"{dominio}:{puerto}", tipo='aviso')
            sleep(2)
    imprimir_mensaje(f"Escuchando websockets...", tipo='info')
    cliente_websocket.wait()
