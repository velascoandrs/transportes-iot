import json

from app.utils import tomar_fotografia_segura


def preparar_datos_envio(id_cabecera, indicesCamara):
    datos = {
        "idCabeceraTrabajoRecorrido": id_cabecera,
    }
    contador = 1
    for indice in indicesCamara:
        imagen = tomar_fotografia_segura(int(indice))
        datos[f'imagen{str(contador)}'] = f"{imagen}"
        contador = contador + 1
    return json.dumps(datos)
