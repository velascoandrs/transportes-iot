import time
from abc import ABC

import paho.mqtt.client as mqtt

from app.microservicios.utils_microservicios import preparar_datos_envio
from app.utils import imprimir_mensaje


class ClienteMQtt(ABC):

    def __init__(self, topic, **config):
        self._topic = topic
        self.opciones = config
        self._cliente = mqtt.Client()
        self._cliente.on_connect = self.ante_conexion
        self._cliente.on_message = self.ante_mensaje
        imprimir_mensaje('Cliente MQTT esta configurado ', tipo='exito')

    def ante_conexion(self, cliente, datos, banderas, rc):
        imprimir_mensaje(f"Se ha subscrito al topic:", self._topic, tipo='exito')
        cliente.subscribe(self._topic)

    def ante_mensaje(self, cliente, datos, mensaje):
        imprimir_mensaje(f"Ha recibido un mensaje:", mensaje.payload, tipo='aviso')

    def enviarMensaje(self, mensaje):
        self._cliente.publish(self._topic, mensaje)
        # self._cliente.loop(timeout=(TIEMPO_ESPERA_DEFECTO + 10))

    def empezar(self):
        uri = f"mqqt://{self.opciones['host']}:{self.opciones['port']}"
        while True:
            imprimir_mensaje('Conectando al broker: ', f'{uri}', tipo='info')
            try:
                self._cliente.connect(
                    host=self.opciones['host'], port=self.opciones['port'],
                    keepalive=self.opciones['keepalive'],
                )
                imprimir_mensaje('Exito al conectar con el broker ', f'{uri}', tipo='exito')
                # self._cliente.subscribe(self._topic)
                self._cliente.subscribe(self._topic)
                self._cliente.loop_start()
                break
            except:
                imprimir_mensaje('Error no se pudo conectar con el broker ', f'{uri}', tipo='error')
                time.sleep(4)
                self._cliente.disconnect()
                self._cliente.unsubscribe(self._topic)
                imprimir_mensaje('reintentando conexion...')


class VehiculoClienteMQTT(ClienteMQtt):
    __id_cabecera_trabajo_recorrido: int = 0

    def definir_cabecera_trabajo_recorrido(self, idCabecera):
        self.__id_cabecera_trabajo_recorrido = idCabecera

    def enviarMensaje(self, indices, prueba=False):
        mensaje = indices
        if not prueba:
            mensaje = preparar_datos_envio(self.__id_cabecera_trabajo_recorrido, indices)
        super(VehiculoClienteMQTT, self).enviarMensaje(mensaje)

    def obtener_cabecera(self):
        return self.__id_cabecera_trabajo_recorrido
