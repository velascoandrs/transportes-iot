from app.microservicios.cliente_mqtt import ClienteMQtt, VehiculoClienteMQTT
from app.settings import CONFIGURACION_MQTT

host = CONFIGURACION_MQTT['dominio']
puerto = CONFIGURACION_MQTT['puerto']

cliente_mqtt_vehiculo = VehiculoClienteMQTT(topic='vehiculo', host=host, port=puerto, keepalive=4)
