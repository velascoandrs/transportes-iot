import argparse
import random
import string
import threading
import time
from functools import reduce

from app.http import cliente_http_configuracion
from app.microservicios import cliente_mqtt_vehiculo
from app.settings import TIEMPO_ESPERA_DEFECTO, TOTAL_CAMARAS
from app.utils import imprimir_mensaje, tomar_fotografia_segura, validar_indices_camaras, validar_argumentos
from app.websocket.utils_ws import inicializar_websockets
from app.websocket.vehiculo_ws import VehiculoClienteWS

parser = argparse.ArgumentParser()
# Se definen argumentos
parser.add_argument("--vehiculo", "-v", help="la placa del vehiculo", required=True)
parser.add_argument("--totalcamaras", "-tc", help="total de camaras", default=TOTAL_CAMARAS)
parser.add_argument("--camara", "-c", help="indices de la camara", default='0,1,2')
argumentos = parser.parse_args()
# Se recupera el vehiculo de los argumentos
PLACA_AGR = argumentos.vehiculo
INDICES_CAMARA_ARG = argumentos.camara.split(',')
TOTAL_CAMARA_ARG = argumentos.totalcamaras
tiempo_espera: int = TIEMPO_ESPERA_DEFECTO


def iniciarWS():
    # En esta parte se inicializan todos los websockets
    inicializar_websockets(
        [
            VehiculoClienteWS('/vehiculo', PLACA_AGR),
        ]
    )


def iniciarMQTT():
    cliente_mqtt_vehiculo.empezar()
    while True:
        time.sleep(tiempo_espera)
        if cliente_mqtt_vehiculo.obtener_cabecera():
            cliente_mqtt_vehiculo.enviarMensaje(INDICES_CAMARA_ARG, prueba=False)
        else:
            cliente_mqtt_vehiculo.enviarMensaje('Esperando cabecera', prueba=True)


def randomString(stringLength=8):
    letters = string.ascii_lowercase
    return ''.join(random.choice(letters) for i in range(stringLength))


def iniciarPeticionHttp():
    try:
        respuesta = cliente_http_configuracion.find_all()
        global tiempo_espera
        if len(respuesta[0]) > 0 and respuesta[0][0]['intervaloImagenTienda']:
            tiempo_espera = respuesta[0][0]['intervaloImagenTienda']
        else:
            tiempo_espera = TIEMPO_ESPERA_DEFECTO
        imprimir_mensaje('Tiempo de espera a utilizar: ', tiempo_espera, tipo='exito')
    except:
        imprimir_mensaje('No se ha podido tener respuesta del servidor ', tipo='error')
        imprimir_mensaje(f'Se va a utilizar el tiempo de espera por defecto: {TIEMPO_ESPERA_DEFECTO}', tipo='aviso')


if __name__ == '__main__':
    validar_argumentos(PLACA_AGR, INDICES_CAMARA_ARG, TOTAL_CAMARA_ARG)
    camaras_conectadas = [tomar_fotografia_segura(int(indice)) for indice in INDICES_CAMARA_ARG]
    estan_todas_conectadas = all(camaras_conectadas)
    if estan_todas_conectadas:
        imprimir_mensaje('Camaras habilitadas: ', f'{INDICES_CAMARA_ARG}')
        iniciarPeticionHttp()
        funciones = [iniciarWS, iniciarMQTT]
        hilos = list()
        imprimir_mensaje('Creando procesos....', tipo='aviso')
        try:

            for funcion in funciones:
                hilo = threading.Thread(target=funcion)
                hilos.append(hilo)
                hilo.start()
            imprimir_mensaje('Procesos Creados con exito!!', tipo='exito')

            for hilo in hilos:
                hilo.join()
            imprimir_mensaje('Los procesos han terminados!!', tipo='aviso')

        except threading.excepthook:
            imprimir_mensaje('Error al crear los hilos', tipo='error')
