## Transportes-IOT

Instalar dependencias:

```shell script
   pip install -r requirements.txt
```

Iniciar programa

```shell script
    python main.py -v <PLACA-SIN-GUION> -tc <TOTAL-CAMARAS> -c <INDICES-CAMARAS>
```

Ejemplo:

```shell script
  python main.py -v PDD3452 -tc 1 -c 0
```
